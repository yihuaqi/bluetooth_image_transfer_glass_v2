package com.example.bluetooth_image_transfer_glass_v2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import com.example.bluetooth_image_transfer_glass_v2.ScreenSurfaceView.OnViewWindowListener;
import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.ImageBuffer;
import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.ImageBufferFactory;
import com.google.android.glass.media.Sounds;

/**
 * This is the main activity that displays the whole interface
 * 
 * @author YiH
 * 
 */
public class BluetoothImage extends Activity implements SensorEventListener,
		GestureDetector.OnGestureListener {

	/** Message types sent from the BluetoothService handler */
	public static final int MESSAGE_IS_RECEIVING = 0;
	public static final int MESSAGE_IS_SENDING = 1;
	public static final int MESSAGE_HAS_SENT = 2;
	public static final int MESSAGE_HAS_RECEIVED = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_INIT = 5;

	public static final String TAG = "image transfer glass";

	/** orientation data */
	public double orientation_azimuth = 180;
	public double orientation_pitch;
	public double orientation_roll;

	/** Count rounds that is turned */
	public int orientation_aimuth_offset;

	/** SurfaceView for displaying screenshot */
	public ScreenSurfaceView mScreenSurfaceView;

	/** Key names received from the BluetoothService handler */
	public static final String DEVICE_NAME = "device_name";

	/** Writing Time cost to files */
	public static BufferedWriter mBufferedWriter;
	public ImageBuffer mImageBuffer;

	private AudioManager mAudioManager;
	private BluetoothImageService mBluetoothImageService = null;
	private BluetoothAdapter mBluetoothAdapter = null;
	private GestureDetector mGestureDetector;

	/** Controlling the scale factor */
	private float scalingFactor = Setting.DEFAULT_SCALE;

	/** If false then doesn't feed orientation data */
	boolean hasReceivedImage = false;
	SensorManager mSensorManager;
	Sensor mOrientation;
	OrientationTransformer ot;
	StringBuffer mStringBuffer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Setting.RECORD_TIMECOST) {
			initBufferWriter();
		}

		// Set the ImageBuffer type.
		ImageBufferFactory.setBufferType(Setting.BUFFER_TYPE);

		// Currently not turning any round.
		orientation_aimuth_offset = 0;
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);

		mScreenSurfaceView = new ScreenSurfaceView(this);
		setContentView(mScreenSurfaceView);

		// init Orientation Sensor
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);

		mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

		// Init OrientationTransformer parameters
		Display display = getWindowManager().getDefaultDisplay();
		ot = OrientationTransformer.getInstance();
		Point size = new Point();
		display.getSize(size);
		ot.setScreenSize(size.x, size.y);
		mScreenSurfaceView.setScreenSize(size.x, size.y);

		// Init GestrueDetector
		mGestureDetector = new GestureDetector(this, this);
		mGestureDetector.setOnDoubleTapListener(mOnDoubleTapListener);

		mImageBuffer = ImageBufferFactory.getBuffer();
		mScreenSurfaceView.setOnRectChangeListener(mChangeListener);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available",
					Toast.LENGTH_SHORT);
			finish();
			return;
		}
		mStringBuffer = new StringBuffer();
		mScreenSurfaceView.setScale(scalingFactor);
	}

	// TODO
	// Might need to move this piece of code to onStart or onCreate
	@Override
	protected void onResume() {
		super.onResume();
		if (!mBluetoothAdapter.isEnabled()) {

		} else {
			if (mBluetoothImageService == null)
				setupTest();
		}

		// Create Bluetooth Connection
		Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
		BluetoothDevice device = devices.iterator().next();
		mBluetoothImageService.connect(device, true);
		mSensorManager.registerListener(this, mOrientation,
				SensorManager.SENSOR_DELAY_GAME);
		Log.d("Orientation_", "Registered");
		hasReceivedImage = true;

	}

	protected void onDestroy() {
		super.onDestroy();
		if (Setting.RECORD_TIMECOST) {
			try {
				mBufferedWriter.flush();
				mBufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (mBluetoothImageService != null)
			mBluetoothImageService.stop();
	}

	protected void onPause() {

		super.onPause();
		mSensorManager.unregisterListener(this);

	}

	private void setupTest() {
		mBluetoothImageService = new BluetoothImageService(this, mHandler);
	}

	/**
	 * Handler for handling the received bluetooth messages.
	 */
	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_IS_RECEIVING:
				mScreenSurfaceView.setText(" Receiving..." + msg.arg2 + "/"
						+ msg.arg1);
				break;
			case MESSAGE_IS_SENDING:
				mScreenSurfaceView
						.setText("Total: " + msg.arg1 + " Sending...");
				break;
			case MESSAGE_HAS_SENT:
				mScreenSurfaceView.setText("All sent: " + msg.arg1);
				break;
			case MESSAGE_INIT:
				// This has been done in BluetoothService

				// int width = msg.arg1;
				// int height = msg.arg2;
				// ot.setImageSize(width, height);
				break;
			case MESSAGE_HAS_RECEIVED:
				hasReceivedImage = true;
				Log.d("Orientation_", "Flag true!!!!!!!!!");

				/*
				 * synchronized (mImageBuffer) {
				 * mScreenSurfaceView.setImageBitmap(mImageBuffer.getImage()); }
				 */

				// mStatusTextView.setText("Received Size: "+mBitmap.getRowBytes()*mBitmap.getWidth());

				break;
			case MESSAGE_DEVICE_NAME:
				mScreenSurfaceView.setText("Connected to "
						+ msg.getData().getString(DEVICE_NAME));
				break;
			case 999:
				Log.d(TAG, "Read " + msg.getData().getString("text"));
				mScreenSurfaceView.setText("Test: "
						+ msg.getData().getString("text"));
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_CAMERA) {
			Log.d("Bluetooth_Landscape", "Captured!!");
			mAudioManager.playSoundEffect(Sounds.SUCCESS);
			mBluetoothImageService.writeOrientationChange();
			Setting.isLandScape = !Setting.isLandScape;
			mScreenSurfaceView.switchMode();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {

		// Counts rounds
		if (event.values[0] + orientation_aimuth_offset * 360
				- orientation_azimuth > 180) {
			orientation_aimuth_offset--;
		}
		if (event.values[0] + orientation_aimuth_offset * 360
				- orientation_azimuth < -180) {
			orientation_aimuth_offset++;
		}

		// Feed data into OrientationTransformer, then get the result and pass
		// to
		// SurfaceView
		orientation_azimuth = event.values[0] + orientation_aimuth_offset * 360;
		orientation_pitch = event.values[1];
		orientation_roll = event.values[2];
		if (hasReceivedImage) {
			double scrollX = ot.getRelativePositionX(orientation_azimuth);
			double scrollY = ot.getRelativePositionY(orientation_pitch);

			mScreenSurfaceView.scrollTo(scrollX, scrollY);

		}

	}

	OnDoubleTapListener mOnDoubleTapListener = new OnDoubleTapListener() {

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			mAudioManager.playSoundEffect(Sounds.TAP);
			Log.d("Double_Tap", "onSingleTapConfirmed");
			// mScreenSurfaceView.enableCursor(!mScreenSurfaceView.isCursorEnabled());
			mBluetoothImageService.writeClick(mScreenSurfaceView.getX(),
					mScreenSurfaceView.getY());
			return true;
		}

		@Override
		public boolean onDoubleTapEvent(MotionEvent e) {
			Log.d("Double_Tap", "onDoubleTapEvent");
			return true;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {

			Log.d("Double_Tap", "onDoubleTap");
			switch (Setting.double_click_action) {
			case Setting.DOUBLE_CLICK_CHANGE_FRAMEBUFFER_INDEX:
				mBluetoothImageService.writeFrameBufferIndexChange();
				mAudioManager.playSoundEffect(Sounds.SUCCESS);
				break;
			case Setting.DOUBLE_CLICK_NO_ACTION:
				return false;

			case Setting.DOUBLE_CLICK_SET_RANGE:
				mAudioManager.playSoundEffect(Sounds.SUCCESS);

				ot.setCorner(orientation_azimuth, orientation_pitch,
						orientation_roll);
				break;

			default:
				break;
			}
			return true;
		}
	};

	@Override
	public boolean onGenericMotionEvent(MotionEvent event) {

		mGestureDetector.onTouchEvent(event);

		return true;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		Log.d("Double_Tap", "onLongPress");

		mAudioManager.playSoundEffect(Sounds.SUCCESS);

		ot.setCenter(orientation_azimuth, orientation_pitch, orientation_roll);
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		Log.d("Double_Tap", "onScroll");
		if (Setting.ALLOW_SCALE) {
			scalingFactor *= (1 - distanceX / 1000);

			mScreenSurfaceView.setScale(scalingFactor);
			mScreenSurfaceView.setText(scalingFactor + "");
		} else {
			mScreenSurfaceView.setScale(Setting.DEFAULT_SCALE);
			mScreenSurfaceView.setText(Setting.DEFAULT_SCALE + "");
		}
		// Log.d("Gesture_test", "Distance X: "+scalingFactor);
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		Log.d("Double_Tap", "onSingleTapUp");

		return true;
	}

	private void initBufferWriter() {
		File subdir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/Sensor_DC/");
		subdir.mkdirs();
		File logfile = new File(subdir, "datalog_" + System.currentTimeMillis());

		if (!logfile.exists()) {
			try {
				logfile.createNewFile();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			mBufferedWriter = new BufferedWriter(new FileWriter(logfile, true));
			if (mBufferedWriter != null) {

			} else {

			}
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private OnViewWindowListener mChangeListener = new OnViewWindowListener() {

		public void OnGridChange(int index, boolean isIntersected) {
			Log.d("Rect_Change", "Changed:" + index + " " + isIntersected);
			mBluetoothImageService.writeGridChange(index, isIntersected);
		}

		@Override
		public void OnWindowChange(int x, int y, int width, int height) {
			mBluetoothImageService.writeWindowChange(x, y, width, height);

		}
	};

}
