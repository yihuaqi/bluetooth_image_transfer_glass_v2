package com.example.bluetooth_image_transfer_glass_v2;

import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.ImageBufferFactory;

public class Setting {

	/** True then will record time cost in local file */
	public static final boolean RECORD_TIMECOST = false;

	/** Determine the buffer type we are going to use. Simple is the best */
	public static final int BUFFER_TYPE = ImageBufferFactory.SIMPLE_BUFFER;

	/** Types of Protocol. */
	public static final int PROTOCAL_SIMPLE = 0;
	public static final int PROTOCAL_FIXED = 1;
	public static final int PROTOCAL_FLEXIBLE = 2;

	/** Determine the buffer area ratio in Flexible Window Protocol */
	public static final double UPDATE_WINDOW_SCALE = 0.25;

	/**
	 * Allow adjusting magnification by swiping forward/backward if true.
	 */
	public static final boolean ALLOW_SCALE = false;

	/**
	 * The default magnification.
	 */
	public static final float DEFAULT_SCALE = (float) (20f / 9f * (Math
			.sqrt(25f * 360f * 640f / 800f / 480f)));
	// public static final float DEFAULT_SCALE = 8;

	/**
	 * Decode the screenshot using landscape mode if true.
	 */
	public static boolean isLandScape = false;

	/**
	 * The action of double click gesture.
	 * 
	 * @see {@link #DOUBLE_CLICK_CHANGE_FRAMEBUFFER_INDEX},
	 *      {@link #DOUBLE_CLICK_NO_ACTION}, {@link #DOUBLE_CLICK_SET_RANGE}
	 */
	public static final int double_click_action = 2;
	/**
	 * Double click action: Setting the range head movement.
	 * 
	 * @see #double_click_action
	 */
	public static final int DOUBLE_CLICK_SET_RANGE = 0;
	/**
	 * Double click action: Changing the framebuffer index.
	 * 
	 * @see #double_click_action
	 */
	public static final int DOUBLE_CLICK_CHANGE_FRAMEBUFFER_INDEX = 1;
	/**
	 * Double click action: No action.
	 * 
	 * @see #double_click_action
	 */
	public static final int DOUBLE_CLICK_NO_ACTION = 2;

	/**
	 * Default range of head horizontal movement
	 */
	public static final int FIXED_WIDTH = 45;
	/**
	 * Default range of head vertical movement.
	 */
	public static final int FIXED_HEIGHT = 15;

	/**
	 * Set this to true can let the user use Glass as left eye device.
	 */
	public static final boolean FOR_LEFT_EYE = false;
	/**
	 * Radius of the cursor size in pixel.
	 */
	public static final int CURSOR_SIZE = 30;
}
