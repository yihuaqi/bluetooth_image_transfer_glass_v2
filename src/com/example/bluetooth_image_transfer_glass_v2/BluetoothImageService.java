package com.example.bluetooth_image_transfer_glass_v2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.ImageBuffer;
import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.ImageBufferFactory;

public class BluetoothImageService {

	// private static final UUID MY_UUID = UUID.fromString(Setting.UUID);

	private final BluetoothAdapter mAdapter;
	private final Handler mHandler;
	private AcceptThread mAcceptThread;

	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;

	private int mState;
	// Name for the SDP record when creating server socket
	private static final String NAME_SECURE = "BluetoothTestSecure";
	private static final String NAME_INSECURE = "BluetoothTestInsecure";

	// Unique UUID for this application
	private static final UUID MY_UUID_SECURE = UUID
			.fromString("fa87c0d0-ffff-11de-8a39-0800200c9a67");
	private static final UUID MY_UUID_INSECURE = UUID
			.fromString("8ce255c0-eeee-11e0-ac64-0800200c9a67");

	// Constants that indicate the current connection state
	public static final int STATE_NONE = 0; // we're doing nothing
	public static final int STATE_LISTEN = 1; // now listening for incoming
												// connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing
													// connection
	public static final int STATE_CONNECTED = 3; // now connected to a remote
													// device
	public static final String TAG = "BS tag";

	public BluetoothImageService(Context context, Handler handler) {
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mHandler = handler;
		// TODO Auto-generated constructor stub
	}

	private void setState(int state) {
		Log.d(TAG, "setState()" + mState + "->" + state);
		mState = state;
	}

	public void start() {
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		setState(STATE_LISTEN);

		if (mAcceptThread == null) {
			mAcceptThread = new AcceptThread(true);
			mAcceptThread.start();
		}

	}

	public void connect(BluetoothDevice device, boolean secure) {
		if (mState == STATE_CONNECTING) {
			if (mConnectedThread != null) {
				mConnectThread.cancle();
				mConnectThread = null;
			}
		}
		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		mConnectThread = new ConnectThread(device, secure);
		mConnectThread.start();
		setState(STATE_CONNECTING);

	}

	public void connected(BluetoothSocket socket, BluetoothDevice device,
			final String socketType) {
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;

		}

		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}
		if (mAcceptThread != null) {
			mAcceptThread.cancle();
			mAcceptThread = null;
		}

		mConnectedThread = new ConnectedThread(socket, socketType);
		mConnectedThread.start();

		Message msg = mHandler
				.obtainMessage(BluetoothImage.MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString(BluetoothImage.DEVICE_NAME, device.getName());
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		setState(STATE_CONNECTED);
	}

	public void stop() {
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		if (mAcceptThread != null) {
			mAcceptThread.cancle();
			mAcceptThread = null;
		}
		setState(STATE_NONE);
	}

	public void write(byte[] out) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.write(out);
	}

	private void connectionFailed() {
		Log.d(TAG, "Connection Failed");
		BluetoothImageService.this.start();
	}

	private void connectionLost() {
		Log.d(TAG, "Connection Lost");
		BluetoothImageService.this.start();
	}

	class AcceptThread extends Thread {
		final BluetoothServerSocket mmServerSocket;
		String mSocketType;

		public AcceptThread(boolean secure) {
			BluetoothServerSocket tmp = null;
			mSocketType = secure ? "Secure" : "Insecure";
			try {
				if (secure) {
					tmp = mAdapter.listenUsingRfcommWithServiceRecord(
							NAME_SECURE, MY_UUID_SECURE);
				} else {
					tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(
							NAME_INSECURE, MY_UUID_INSECURE);
				}

			} catch (IOException e) {
				Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
			}
			mmServerSocket = tmp;
		}

		public void run() {
			setName("AcceptThread" + mSocketType);
			BluetoothSocket socket = null;

			while (mState != STATE_CONNECTED) {
				try {
					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.e(TAG, "Socket Type: " + mSocketType
							+ "accept() failed", e);
					break;
				}

				if (socket != null) {
					synchronized (BluetoothImageService.this) {
						switch (mState) {
						case STATE_LISTEN:
						case STATE_CONNECTING:
							// Situation normal. Start the connected thread.
							connected(socket, socket.getRemoteDevice(),
									mSocketType);
							break;
						case STATE_NONE:
						case STATE_CONNECTED:
							// Either not ready or already connected. Terminate
							// new socket.
							try {
								socket.close();
							} catch (IOException e) {
								Log.e(TAG, "Could not close unwanted socket", e);
							}
							break;
						}

					}
				}

			}
		}

		public void cancle() {
			try {
				mmServerSocket.close();

			} catch (IOException e) {
				Log.e(TAG, "Socket Type" + mSocketType
						+ "close() of server failed", e);
			}

		}

	}

	class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;
		private String mSocketType;

		public ConnectThread(BluetoothDevice device, boolean secure) {
			mmDevice = device;
			BluetoothSocket tmp = null;
			mSocketType = secure ? "Secure" : "Insecure";

			try {
				if (secure) {
					tmp = device
							.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
				} else {
					tmp = device
							.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
				}
			} catch (IOException e) {
				Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
			}
			mmSocket = tmp;
		}

		public void run() {
			setName("ConnectThread" + mSocketType);
			mAdapter.cancelDiscovery();
			try {
				mmSocket.connect();
			} catch (IOException e) {
				try {
					mmSocket.close();
				} catch (IOException e2) {
					Log.e(TAG, "unable to close() " + mSocketType
							+ " socket during connection failure", e2);
				}
				connectionFailed();
				return;
			}

			synchronized (BluetoothImageService.this) {
				mConnectThread = null;

			}

			connected(mmSocket, mmDevice, mSocketType);
		}

		public void cancle() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect " + mSocketType
						+ " socket failed", e);
			}
		}

	}

	class ConnectedThread extends Thread {
		boolean hasGotSize = false;
		private final int WAITING = 0;
		private final int RECEIVING_GRID = 1;
		private final int RECEIVING_INIT = 2;
		int size = 0;
		int current_position = 0;
		byte[] image;
		int state;
		char stateChar;
		int index;
		private int PROTOCAL = Setting.PROTOCAL_SIMPLE;
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		private final DataInputStream dis;
		private final DataOutputStream dos;
		private ImageBuffer mImageBuffer;

		public ConnectedThread(BluetoothSocket socket, String socketType) {
			Log.d(TAG, "create ConnectedThread: " + socketType);
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			mImageBuffer = ImageBufferFactory.getBuffer();
			// Get the BluetoothSocket input and output streams
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();

			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
			dis = new DataInputStream(mmInStream);
			dos = new DataOutputStream(mmOutStream);
			state = WAITING;
		}

		public void run() {

			while (true) {

				try {
					switch (state) {
					case WAITING:
						stateChar = dis.readChar();
						Log.d("Read_logic", "Read char:" + stateChar);
						if (stateChar == 'g') {
							state = RECEIVING_GRID;
						}
						if (stateChar == 'i') {
							state = RECEIVING_INIT;
						}
						break;
					case RECEIVING_GRID:
						mImageBuffer.receiveGrid(dis);
						mHandler.obtainMessage(
								BluetoothImage.MESSAGE_HAS_RECEIVED, 0, 0)
								.sendToTarget();
						Log.d("Orientation_", "Receive Grid!!!!!!!!!!!!");

						state = WAITING;

						break;

					/**
					 * Receive initialization parameters
					 */
					case RECEIVING_INIT:
						PROTOCAL = dis.readInt();
						int width = dis.readInt();
						int height = dis.readInt();
						mImageBuffer.init(width, height);
						OrientationTransformer.getInstance().setImageSize(
								width, height);
						ScreenSurfaceView.initBitmap(width, height);

						ScreenSurfaceView.PROTOCAL = PROTOCAL;
						mImageBuffer.setProtocol(PROTOCAL);
						switch (PROTOCAL) {
						case Setting.PROTOCAL_SIMPLE:

							break;
						case Setting.PROTOCAL_FIXED:
							int rows = dis.readInt();
							int cols = dis.readInt();
							ScreenSurfaceView.setGridDimension(rows, cols);

							break;
						case Setting.PROTOCAL_FLEXIBLE:

							break;
						default:
							break;
						}

						// image = new byte[width*height];

						state = WAITING;

						break;
					}
				} catch (IOException e) {
					connectionLost();
					// Start the service over to restart listening mode
					// BluetoothImageService.this.start();
					break;
				}

			}

		}

		public void write(byte[] buffer) {

			try {
				mmOutStream.write(buffer);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void cancle() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}

		}

		public void writeGridChange(int index2, boolean focused) {
			try {
				dos.writeChar('g');
				dos.writeInt(index2);
				dos.writeBoolean(focused);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		public void writeWindowChange(int x, int y, int width, int height) {
			try {
				dos.writeChar('w');
				dos.writeInt(x);
				dos.writeInt(y);
				dos.writeInt(width);
				dos.writeInt(height);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		public void writeClick(float x, float y) {
			try {
				dos.writeChar('c');
				dos.writeFloat(x);
				dos.writeFloat(y);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		public void writeOrientationChange() {
			try {
				dos.writeChar('o');

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		public void writeFrameBufferIndexChange() {
			try {
				dos.writeChar('f');

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	/*
	 * public void sendImage(Resources r,int test) { Log.d("Image_Size",
	 * "Start Decoding"); BluetoothImage.isSending = true; Bitmap bm =
	 * BitmapFactory.decodeResource(r, R.drawable.test); Log.d("Image_Size",
	 * "Decoding complete"); ByteArrayOutputStream bytes = new
	 * ByteArrayOutputStream(); //int size = bm.getRowBytes()*bm.getHeight();
	 * //Log.d("Image_Size", "Allocated: "+size+""); //ByteBuffer buffer =
	 * ByteBuffer.allocate(size); //bm.copyPixelsToBuffer(buffer);
	 * Log.d("Image_Size", "Start Compressing");
	 * bm.compress(Bitmap.CompressFormat.PNG, 100, bytes); Log.d("Image_Size",
	 * "Compressing complete"); bm.recycle();
	 * 
	 * byte[] image = bytes.toByteArray(); Log.d("Image_Size",
	 * "Image_Size"+image.length+""); Log.d("Image_Size",
	 * "Start Writing image"); write(image); Log.d("Image_Size",
	 * "Finish writing image");
	 * 
	 * }
	 */

	/**
	 * inform the mobile the grid index has changed.
	 * 
	 * @param index
	 * @param focused
	 */
	public void writeGridChange(int index, boolean focused) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeGridChange(index, focused);
	}

	/**
	 * inform the mobile the viewing window has changed.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public void writeWindowChange(int x, int y, int width, int height) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeWindowChange(x, y, width, height);

	}

	/**
	 * Send a click event to mobile
	 * 
	 * @param x
	 * @param y
	 */
	public void writeClick(float x, float y) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeClick(x, y);

	}

	/**
	 * Inform the mobile to change its encoding mode.
	 */
	public void writeOrientationChange() {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeOrientationChange();

	}

	/**
	 * Inform the mobile to change its framebuffer index
	 */
	public void writeFrameBufferIndexChange() {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeFrameBufferIndexChange();

	}

}
