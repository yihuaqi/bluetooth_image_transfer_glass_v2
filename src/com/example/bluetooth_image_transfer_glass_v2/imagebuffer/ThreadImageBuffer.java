package com.example.bluetooth_image_transfer_glass_v2.imagebuffer;

import java.io.DataInputStream;
import java.io.IOException;

import com.example.bluetooth_image_transfer_glass_v2.ScreenSurfaceView;
import com.example.bluetooth_image_transfer_glass_v2.Setting;

import android.util.Log;

/**
 * This is a Thread version of ImageBufer implementation.
 * In the background a decoding thread is running. It will wait() until
 * there is undecoded Bitmap in the buffer. 
 * @author YiH
 *
 */
public class ThreadImageBuffer implements ImageBuffer{
	
	// Double buffer.
	GridImage[] mGridImages = new GridImage[2];
	private int freeBufferIndex = 0;
	static ImageBuffer instance;
	byte[] image;
	int protocal;
	@Override
	public void setProtocol(int protocal) {
		this.protocal = protocal;
		
	}
	private ThreadImageBuffer() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				GridImage gi;
				int i = 0;
				
				while(true){
					gi = mGridImages[i];
					if(gi!=null){
						Log.d("Thread_image", "Decode");
						synchronized (gi) {
							while(gi.hasDecoded){
								try {
									Log.d("Thread_image","wait gridImage "+i);
									gi.wait();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							long start = System.currentTimeMillis();
							gi.decode();
							Log.d("Speed_test","Decode:"+(System.currentTimeMillis()-start));
							i = (i+1)%2;
						}
						
					}
				}
			}
		}).start();
	}
	public static ImageBuffer getInstance(){
		if(instance==null){
			instance = new ThreadImageBuffer();
		}
		return instance;
	}
	
	/**
	 * Push the GridImage to Buffer.
	 * @param temp the GridImage that is going to be pushed
	 */
	public void pushGridImage(GridImage temp) {
		int i = freeBufferIndex;
		while(true){
			if(mGridImages[i]==null){
				Log.d("Thread_image", "Create new GridImage "+i);
				mGridImages[i] = temp;
				freeBufferIndex = (freeBufferIndex+1)%2;
			}else if(mGridImages[i]!=null || (mGridImages[i].hasDecoded&&!mGridImages[i].isNewImage)){
				Log.d("Thread_image", "Set GridImage "+i);
				synchronized (mGridImages[i]) {
					mGridImages[i].setGridImage(temp);
					Log.d("Thread_image","notify gridImage "+i);
					mGridImages[i].notify();
				}
				freeBufferIndex = (freeBufferIndex+1)%2;
				return;
			}
			
		}
		
	}

	@Override
	public GridImage getImage() {
		GridImage gi;
		int i = (freeBufferIndex+1)%2;
		
		 gi = mGridImages[i];
		 if(gi!=null){
			 

			 if(gi.hasDecoded&&gi.isNewImage){
				 synchronized (gi) {
				 gi.isNewImage = false;
				 
				 return gi;
				 }
			}
		 }
	
		
		return null;
	}

	/**
	 * Receive GridImage in Flexible Window Protocol
	 * @param dis DataInputStream that is streaming GridImage data
	 * @throws IOException Bluetooth read fails
	 */
	private void receiveFlexibleImage(DataInputStream dis) throws IOException{
		
		
		int size = dis.readInt();
		image = new byte[size];
		long start = System.currentTimeMillis();
		dis.readFully(image, 0, size);
		long end = System.currentTimeMillis();
		Log.d("FLEXIBLE_SPEED","Read:"+ScreenSurfaceView.scale+":"+(end-start));
		int x = dis.readInt();
		int y = dis.readInt();
		pushGridImage(new GridImage(image, 0, size,x,y));
		//pushGridImage(image, 0, size, x,y);
		
	}

	/**
	 * Receive Gridimage in Fixed Window protocol
	 * @param dis DataInputStream that is streaming GridImage data
	 * @throws IOException Bluetooth read fails
	 */
	private void receiveFixedImage(DataInputStream dis)  throws IOException{
		int size = dis.readInt();
		image = new byte[size];

		dis.readFully(image, 0, size);
		int index = dis.readInt();
		
		pushGridImage(new GridImage(image, 0, size,index));
		
	}
	
	/**
	 * Receive GridImage in Simple Protocol
	 * @param dis DataInputStream that is streaming GridImage data
	 * @throws IOException Bluetooth read fails
	 */
	private void receiveSimpleImage(DataInputStream dis)  throws IOException{
		int size = dis.readInt();
		image = new byte[size];
		dis.readFully(image, 0, size);
		
		pushGridImage(new GridImage(image, 0, size));
	}
	@Override
	public void receiveGrid(DataInputStream dis) throws IOException {
		
		switch (protocal) {
		case Setting.PROTOCAL_SIMPLE:
			receiveSimpleImage(dis);
			break;
		case Setting.PROTOCAL_FIXED:
			receiveFixedImage(dis);
			break;
		case Setting.PROTOCAL_FLEXIBLE:
			receiveFlexibleImage(dis);
			break;

		default:
			break;
		}
	
	}

	@Override
	public void init(int width, int height) throws IOException {
		return;
		
	}

}
