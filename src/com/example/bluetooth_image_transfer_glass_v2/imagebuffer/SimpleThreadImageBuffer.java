package com.example.bluetooth_image_transfer_glass_v2.imagebuffer;

import java.io.DataInputStream;
import java.io.IOException;

import com.example.bluetooth_image_transfer_glass_v2.OrientationTransformer;
import com.example.bluetooth_image_transfer_glass_v2.ScreenSurfaceView;
import com.example.bluetooth_image_transfer_glass_v2.Setting;

import android.graphics.Bitmap;
import android.util.Log;
/**
 * This is an implementation of {@link ImageBuffer} that puts decoding in another thread
 * so that it will return immediately when pushGridImage is called. But the decoding thread
 * is using a naive while loop which wastes a lot of cup time and thus is not better than 
 * {@link SimpleImageBuffer}.
 * @author YiH
 *
 */
public class SimpleThreadImageBuffer implements ImageBuffer{
	GridImage gi;
	byte[] image;
	ScreenSurfaceView mScreenSurfaceView;
	static SimpleThreadImageBuffer instance;
	int protocal;
	private SimpleThreadImageBuffer() {
		Log.d("Decoding_Thread", "Start");
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
				
					if(gi!=null && !gi.hasDecoded){
						long start = System.currentTimeMillis();
						gi.decode();
						Log.d("Speed_test", "Decode:"+(System.currentTimeMillis()-start));
						//Log.d("Speed_test", "Decoded");
					}
				
				}
			}
		}).start();
	}
	@Override
	public void setProtocol(int protocal) {
		this.protocal = protocal;
		
	}
	
	/**
	 * Push the GridImage to Buffer
	 * @param temp the GridImage that is going to be pushed
	 */
	public void pushGridImage(GridImage temp) {
		Log.d("ImageBuffer", "Image Pushed");
	
		gi = temp;
		gi.isNewImage = true;
		
		
	}

	@Override
	public GridImage getImage() {
		if(gi!=null&&gi.isNewImage&&gi.hasDecoded){
			Log.d("ImageBuffer", "Image returned");
			gi.isNewImage = false;
			return gi;
		}
		return null;
	}

	/**
	 * Get a singleton instance of SimpleImageBuffer
	 * @return
	 */
	static public ImageBuffer getInstance() {
		if(instance==null){
			instance = new SimpleThreadImageBuffer();
			setDimentions(1, 1);
		}
		return instance;
	}
	
	static public void setDimentions(int col, int row) {
		return;
		
	}

	/**
	 * Receive GridImage in Flexible Window Protocol
	 * @param dis DataInputStream that is streaming GridImage data
	 * @throws IOException Bluetooth read fails
	 */
	private void receiveFlexibleImage(DataInputStream dis) throws IOException{
		
		
		int size = dis.readInt();
		image = new byte[size];

		long start = System.currentTimeMillis();
		dis.readFully(image, 0, size);
		long end = System.currentTimeMillis();
		Log.d("FLEXIBLE_SPEED","Read:"+ScreenSurfaceView.scale+":"+(end-start));
		int x = dis.readInt();
		int y = dis.readInt();
		pushGridImage(new GridImage(image, 0, size,x,y));
		//pushGridImage(image, 0, size, x,y);
		
	}

	/**
	 * Receive Gridimage in Fixed Window protocol
	 * @param dis DataInputStream that is streaming GridImage data
	 * @throws IOException Bluetooth read fails
	 */
	private void receiveFixedImage(DataInputStream dis)  throws IOException{
		int size = dis.readInt();
		image = new byte[size];

		dis.readFully(image, 0, size);
		int index = dis.readInt();
		
		pushGridImage(new GridImage(image, 0, size,index));
		
	}
	
	/**
	 * Receive GridImage in Simple Protocol
	 * @param dis DataInputStream that is streaming GridImage data
	 * @throws IOException Bluetooth read fails
	 */
	private void receiveSimpleImage(DataInputStream dis)  throws IOException{
		int size = dis.readInt();
		image = new byte[size];
		dis.readFully(image, 0, size);
		
		pushGridImage(new GridImage(image, 0, size));
	}
	
	@Override
	public void receiveGrid(DataInputStream dis) throws IOException {
		
		switch (protocal) {
		case Setting.PROTOCAL_SIMPLE:
			receiveSimpleImage(dis);
			break;
		case Setting.PROTOCAL_FIXED:
			receiveFixedImage(dis);
			break;
		case Setting.PROTOCAL_FLEXIBLE:
			receiveFlexibleImage(dis);
			break;

		default:
			break;
		}
	}
	
	@Override
	public void init(int width,int height) throws IOException {

	}

}
