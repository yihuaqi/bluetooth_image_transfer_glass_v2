package com.example.bluetooth_image_transfer_glass_v2.imagebuffer;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PushbackInputStream;

import com.example.bluetooth_image_transfer_glass_v2.BluetoothImageService;
import com.example.bluetooth_image_transfer_glass_v2.ScreenSurfaceView;

import android.bluetooth.BluetoothServerSocket;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.sax.StartElementListener;
import android.text.InputFilter.LengthFilter;
import android.text.style.LineHeightSpan.WithDensity;
/**
 * This interface is a buffer that is used to store the {@link GridImage}, decode them,
 * and supply GridImage to rendering thread.  
 * @author YiH
 *
 */
public interface ImageBuffer {
	
	/**
	 * Pop a {@link GridImage} from the buffer.
	 * The returned GridImage can be used for rendering
	 * Return null if there is no GridImage or the GridImage has already been popped
	 * or the GridImage has not been decoded yet.
	 * 
	 * @return A {@link GridImage} in the buffer.
	 * 			<code>null</code> if there is no GridImage in the  buffer or 
	 * 			the GridImage has already been popped or the GridImage has not 
	 * 			been decoded yet.
	 */
	GridImage getImage();
	
	/**
	 * Takes a {@link DataInputStream} from bluetooth and handle the reading of receiveGrid.
	 * Inside this will call pushGridImage.
	 * 
	 * @param dis the DatainputStream from {@link BluetoothImageService}.
	 * @throws IOException If an input or output exception occurs.
	 */
	void receiveGrid(DataInputStream dis) throws IOException;
	/**
	 * Does some initialization work with the whole screen image width and height.
	 *  
	 * @param width the width of the screenshot
	 * @param height the height of the screenshot
	 * @throws IOException If an input or output exception occurs
	 */
	void init(int width,int height) throws IOException;
	
	/**
	 * Set the protocol.
	 * @param protocol
	 */
	void setProtocol(int protocol);
}
