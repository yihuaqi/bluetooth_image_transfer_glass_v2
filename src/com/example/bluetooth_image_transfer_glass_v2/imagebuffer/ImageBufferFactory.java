package com.example.bluetooth_image_transfer_glass_v2.imagebuffer;

/**
 * Factory class for getting different ImageBuffer
 * @author YiH
 *
 */
public class ImageBufferFactory {
	public static final int SIMPLE_BUFFER = 0; 
	public static final int SIMPLE_THREAD_BUFFER = 1;
	public static final int THREAD_BUFFER =2;
	static int bufferType = 0;
	public static void setBufferType(int type){
		bufferType = type;
	}
	
	public static ImageBuffer getBuffer(){
		return produce(bufferType);
	}
	
	private static ImageBuffer produce(int type){
		if(type==SIMPLE_BUFFER){
			return SimpleImageBuffer.getInstance();
		}
		if(type==SIMPLE_THREAD_BUFFER){
			return SimpleThreadImageBuffer.getInstance();
		}
		if(type==THREAD_BUFFER){
			return ThreadImageBuffer.getInstance();
		}
		return null;
	}
}
