package com.example.bluetooth_image_transfer_glass_v2.imagebuffer;

import java.io.ByteArrayOutputStream;

import com.example.bluetooth_image_transfer_glass_v2.ScreenSurfaceView;
import com.example.bluetooth_image_transfer_glass_v2.Setting;



import android.R.integer;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class GridImage {
	byte[] image;
	int offset;
	int length;
	int index;
	Bitmap bm;
	boolean isNewImage = true;
	boolean hasDecoded = false;
	int x;
	int y;
	
	public GridImage(byte[] bytes,int o,int l,int i) {
		image = bytes;
		offset=o;
		length=l;
		index=i;
		hasDecoded = false;
		isNewImage = true;
	}
	public GridImage(byte[] bytes, int o, int l) {
		image = bytes;
		offset=o;
		length=l;
		hasDecoded = false;
		isNewImage = true;
	}
	public GridImage(byte[] bytes, int o, int l, int x2, int y2) {
		image = bytes;
		offset=o;
		length=l;
		x=x2;
		y=y2;
		hasDecoded = false;
		isNewImage = true;
	}
	public int getIndex(){
		return index;
		
	}
	public void decode(){
		
		
		long s = System.currentTimeMillis();
		bm = BitmapFactory.decodeByteArray(image,offset,length);
		long e = System.currentTimeMillis();
		Log.d("FLEXIBLE_SPEED","Decode:"+ScreenSurfaceView.scale+":"+(e-s));
		hasDecoded = true;
		
	
	}
	
	public void dummyDecode(){
		hasDecoded = true;
	}
	public Bitmap getBitmap(){
		if(!hasDecoded){
			return null;
		}
		return bm;
	}
	public void setGridImage(byte[] image2, int start, int length2, int index2) {
		image = image2;
		offset = start;
		length = length2;
		index = index2;
		hasDecoded = false;
		isNewImage = true;
		
	}
	
	public void setGridImage(GridImage temp){
		image = temp.image;
		offset = temp.offset;
		length = temp.length;
		index = temp.index;
		x = temp.x;
		y = temp.y;
		hasDecoded = false;
		isNewImage = true;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
}
