package com.example.bluetooth_image_transfer_glass_v2.imagebuffer;

import java.io.DataInputStream;
import java.io.IOException;

import android.util.Log;

import com.example.bluetooth_image_transfer_glass_v2.ScreenSurfaceView;
import com.example.bluetooth_image_transfer_glass_v2.Setting;

/**
 * This is a simple implementation of {@link ImageBuffer}. When receiving image,
 * this will block the thread calling receiveGrid until the image is decoded.
 * 
 * @author YiH
 * 
 */
public class SimpleImageBuffer implements ImageBuffer {
	GridImage gi;
	byte[] image;
	ScreenSurfaceView mScreenSurfaceView;

	static SimpleImageBuffer instance;
	int protocal;

	private SimpleImageBuffer() {

	}

	/**
	 * Push a GridImage to the Buffer
	 * 
	 * @param input
	 *            GridImage that is going to be pushed
	 */
	private synchronized void pushGridImage(GridImage input) {
		Log.d("ImageBuffer", "Image Pushed");
		gi = input;
		long start = System.currentTimeMillis();
		gi.decode();

		gi.isNewImage = true;

	}

	@Override
	public synchronized GridImage getImage() {

		if (gi != null && gi.isNewImage) {
			gi.isNewImage = false;

			return gi;
		}

		return null;

	}

	/**
	 * Get a singleton instance of SimpleImageBuffer
	 * 
	 * @return
	 */
	static public ImageBuffer getInstance() {
		if (instance == null) {
			instance = new SimpleImageBuffer();
			setDimentions(1, 1);
		}
		return instance;
	}

	static public void setDimentions(int col, int row) {
		return;

	}

	@Override
	public void receiveGrid(DataInputStream dis) throws IOException {

		switch (protocal) {
		case Setting.PROTOCAL_SIMPLE:
			receiveSimpleImage(dis);
			break;
		case Setting.PROTOCAL_FIXED:
			receiveFixedImage(dis);
			break;
		case Setting.PROTOCAL_FLEXIBLE:
			receiveFlexibleImage(dis);
			break;

		default:
			break;
		}

	}

	/**
	 * Receive Image in Flexible Window Protocol
	 * 
	 * @param dis
	 *            DataInputStream that is streaming GridImage Data
	 * @throws IOException
	 *             Bluetooth read fails
	 */
	private void receiveFlexibleImage(DataInputStream dis) throws IOException {

		int size = dis.readInt();
		image = new byte[size];

		long start = System.currentTimeMillis();
		dis.readFully(image, 0, size);
		long end = System.currentTimeMillis();
		if (end != start) {
			// Log.d("FLEXIBLE_SPEED","Read:"+(end-start)+":"+(size/(end-start))+"Kb/s");
			Log.d("FLEXIBLE_SPEED", "Read:" + ScreenSurfaceView.scale + ":"
					+ (end - start));
		}
		int x = dis.readInt();
		int y = dis.readInt();
		pushGridImage(new GridImage(image, 0, size, x, y));
		// pushGridImage(image, 0, size, x,y);

	}

	/**
	 * Receive Image in Fixed Window Protocol
	 * 
	 * @param dis
	 *            DataInputStream that is streaming GridImage data
	 * @throws IOException
	 *             Bluetooth read fails
	 */
	private void receiveFixedImage(DataInputStream dis) throws IOException {
		int size = dis.readInt();
		image = new byte[size];

		dis.readFully(image, 0, size);
		int index = dis.readInt();

		pushGridImage(new GridImage(image, 0, size, index));

	}

	/**
	 * Receive Image in Simple Protocol
	 * 
	 * @param dis
	 *            DataInputStream that is streaming GridImage data
	 * @throws IOException
	 *             Bluetooth read fails
	 */
	private void receiveSimpleImage(DataInputStream dis) throws IOException {
		int size = dis.readInt();
		image = new byte[size];
		long start = System.currentTimeMillis();
		dis.readFully(image, 0, size);
		long end = System.currentTimeMillis();
		if (end != start) {
			Log.d("FLEXIBLE_SPEED", "Read:" + (end - start) + ":"
					+ (size / (end - start)) + "Kb/s");
		}
		pushGridImage(new GridImage(image, 0, size));
	}

	@Override
	public void init(int width, int height) throws IOException {

	}

	@Override
	public void setProtocol(int protocal) {
		this.protocal = protocal;

	}

}
