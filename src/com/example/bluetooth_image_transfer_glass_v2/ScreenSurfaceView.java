package com.example.bluetooth_image_transfer_glass_v2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.GridImage;
import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.ImageBuffer;
import com.example.bluetooth_image_transfer_glass_v2.imagebuffer.ImageBufferFactory;

/**
 * ScreenSurfaceView is used to show ScreenShot
 * 
 * @author YiH
 * 
 */
public class ScreenSurfaceView extends SurfaceView implements
		SurfaceHolder.Callback {

	/** Scroll value */
	private double scrollX = 0;
	private double scrollY = 0;

	/** Scale value */
	static public float scale = 1;

	/** Debug text */
	private int textColor = Color.YELLOW;
	private String statusText = "Status";
	Paint statusPaint;

	/** Thread for updating screen */
	private MySurfaceThread thread;

	/** Control the frame rate */
	private int SLEEP_TIME = 33;

	private Rect srcRect;
	private Rect dstRect;
	private static int bitmapWidth = 480;
	private static int bitmapHeight = 800;
	private int screenWidth;
	private int screenHeight;

	/** bitmap that will be drawn on the canvas */
	private static volatile Bitmap portraitbitmap = Bitmap.createBitmap(
			bitmapWidth, bitmapHeight, Config.ARGB_8888);
	private static volatile Bitmap landscapeBitmap = Bitmap.createBitmap(
			bitmapHeight, bitmapWidth, Config.ARGB_8888);
	private static volatile Bitmap bitmap = null;
	private ImageBuffer mImageBuffer;
	private OnViewWindowListener mOnGridChangeListener;
	private static List<Rect> mGridRects;
	private static boolean[] intersectRects;
	private static int mGridCols;

	/** Debug for Fixed Window Protocol */
	String TAG = "surface_view";
	Paint rectIntersectedPaint;
	Paint rectNotIntersectedPaint;
	Paint srcPaint;
	public static int PROTOCAL = -1;
	Rect updateRect;
	private Paint cursorPaint;
	private boolean drawCursor = true;

	/**
	 * Whenever there is a change of Rects showing on screen, an OnRectChange
	 * event will be fired to the listener.
	 * 
	 * @author YiH
	 * 
	 */
	public interface OnViewWindowListener {
		/**
		 * This method will be called whenever there is a change of Rects
		 * showing on screen.
		 * 
		 * @param indexboolean
		 *            the index of changed grid
		 * @param isIntersected
		 */
		void OnGridChange(int indexboolean, boolean isIntersected);

		/**
		 * This will be called whenever the viewing window changes.
		 * 
		 * @param x
		 *            x coordinate of start point
		 * @param y
		 *            y coordinate of start point
		 * @param width
		 *            width of the viewing window
		 * @param height
		 *            height of the viewing window
		 */
		void OnWindowChange(int x, int y, int width, int height);
	}

	/**
	 * Set the OnRectChangeListener
	 * 
	 * @param l
	 */
	public void setOnRectChangeListener(OnViewWindowListener l) {
		mOnGridChangeListener = l;
	}

	/**
	 * Constructor.
	 * 
	 * @param context
	 */
	public ScreenSurfaceView(Context context) {
		super(context);
		init();
	}

	private void init() {
		getHolder().addCallback(this);
		statusPaint = new Paint();
		statusPaint.setColor(textColor);
		statusPaint.setTextAlign(Align.LEFT);
		statusPaint.setTextSize(40);
		rectIntersectedPaint = new Paint();
		rectIntersectedPaint.setColor(Color.RED);
		rectIntersectedPaint.setStyle(Style.STROKE);
		rectNotIntersectedPaint = new Paint();
		rectNotIntersectedPaint.setColor(Color.BLUE);
		rectNotIntersectedPaint.setStyle(Style.STROKE);
		srcPaint = new Paint();
		srcPaint.setColor(Color.YELLOW);
		srcPaint.setStyle(Style.STROKE);
		cursorPaint = new Paint();
		cursorPaint.setColor(Color.RED);

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		startRendering();

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		stopRendering();

	}

	/**
	 * Set the text that is shown on the top of the screen
	 * 
	 * @param s
	 *            The text that is going to be shown
	 */
	public void setText(String s) {
		statusText = s;

	}

	@Override
	public void draw(Canvas canvas) {

		mImageBuffer = ImageBufferFactory.getBuffer();

		switch (PROTOCAL) {
		case Setting.PROTOCAL_SIMPLE:
			setSimpleImageBitmap(mImageBuffer.getImage());
			break;
		case Setting.PROTOCAL_FIXED:
			setFixedImageBitmap(mImageBuffer.getImage());
			break;
		case Setting.PROTOCAL_FLEXIBLE:
			setFlexibleImageBitmap(mImageBuffer.getImage());
			break;

		default:
			break;
		}

		// Repaint the canvas as Black
		// canvas.drawARGB(255, 0, 0, 0);
		canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);

		// Draw the screenshot
		if (bitmap != null) {
			updataSrcRect();
			if (Setting.FOR_LEFT_EYE) {

				canvas.save(Canvas.MATRIX_SAVE_FLAG);
				canvas.rotate(180, screenWidth / 2, screenHeight / 2);
				canvas.drawBitmap(bitmap, srcRect, dstRect, null);
				canvas.restore();
			} else {
				canvas.drawBitmap(bitmap, srcRect, dstRect, null);
			}
		}

		// Debug information for drawing the grids.
		if (PROTOCAL == Setting.PROTOCAL_FIXED) {
			if (mGridRects != null) {
				for (int i = 0; i < mGridRects.size(); i++) {
					if (intersectRects[i]) {
						canvas.drawRect(mGridRects.get(i), rectIntersectedPaint);
					} else {
						canvas.drawRect(mGridRects.get(i),
								rectNotIntersectedPaint);
					}
				}
			}
			if (srcRect != null) {
				canvas.drawRect(srcRect, srcPaint);
			}
		}

		// Draw cursor in the center of screen.
		if (drawCursor) {
			canvas.drawCircle(screenWidth / 2, screenHeight / 2,
					Setting.CURSOR_SIZE, cursorPaint);
		}
		canvas.drawText(statusText, 20, 50, statusPaint);

	}

	/**
	 * Replace the Bitmap with the new one.
	 * 
	 * @param mGridImage
	 */
	private void setSimpleImageBitmap(GridImage mGridImage) {
		if (mGridImage != null) {
			if (mGridImage.getBitmap() != null) {

				Bitmap b = mGridImage.getBitmap();
				bitmap = b;
				// c = new int[bitmapHeight*bitmapWidth];
				// b.getPixels(c, 0, bitmapWidth, 0, 0, bitmapWidth,
				// bitmapHeight);
				// bitmap.setPixels(c, 0, bitmapWidth, 0, 0, bitmapWidth,
				// bitmapHeight);

			}
		}

	}

	/**
	 * Given the x and y coordinates, scroll the canvas to the given position.
	 * 
	 * @param scrollX2
	 * @param scrollY2
	 */
	public void scrollTo(double scrollX2, double scrollY2) {

		scrollX = scrollX2;
		scrollY = scrollY2;

	}

	/**
	 * Set the scale factor.
	 * 
	 * @param s
	 *            The scale factor.
	 */
	public void setScale(float s) {

		scale = s;

		// matrix.setScale(s, s);
		// Log.d(TAG, "Set scale to s");
	}

	/**
	 * 
	 * Given a {@link GridImage}, update the bitmap based on this gridImage.
	 * 
	 * @param mGridImage
	 *            The gridImage that is used to update.
	 */
	public void setFixedImageBitmap(GridImage mGridImage) {
		int[] c;
		if (mGridImage != null) {
			if (mGridImage.getBitmap() != null) {
				if (mGridImage.getIndex() == -1) {
					Bitmap b = mGridImage.getBitmap();
					c = new int[bitmapHeight * bitmapWidth];
					b.getPixels(c, 0, bitmapWidth, 0, 0, bitmapWidth,
							bitmapHeight);
					bitmap.setPixels(c, 0, bitmapWidth, 0, 0, bitmapWidth,
							bitmapHeight);
				} else {

					Log.d("ImageBuffer", "Update");
					Bitmap b = mGridImage.getBitmap();
					OrientationTransformer.getInstance();

					if (srcRect == null) {
						updataSrcRect();
					}

					// c = new int[b.getByteCount()/4];
					int bWidth = b.getWidth();
					int bHeight = b.getHeight();
					c = new int[bWidth * bHeight];
					int index = mGridImage.getIndex();
					int row = index / mGridCols;
					int col = index % mGridCols;
					int x = col * bWidth;
					int y = row * bHeight;
					Log.d(TAG, "Initialize colors[]");
					b.getPixels(c, 0, bWidth, 0, 0, bWidth, bHeight);
					Log.d("FOCUS_TEST_GLASS", "Rendering Grid#" + index);

					bitmap.setPixels(c, 0, bWidth, x, y, bWidth, bHeight);

				}

			}
		}
	}

	/**
	 * Update the Bitmap with given GridImage.
	 * 
	 * @param mGridImage
	 */
	private void setFlexibleImageBitmap(GridImage mGridImage) {
		int[] c;
		if (mGridImage != null) {
			if (mGridImage.getBitmap() != null) {
				Bitmap b = mGridImage.getBitmap();
				if (srcRect == null) {
					updataSrcRect();
				}
				Log.d("Frame_Rate", System.currentTimeMillis() + "");
				int bWidth = b.getWidth();
				int bHeight = b.getHeight();
				c = new int[bWidth * bHeight];
				int x = mGridImage.getX();
				int y = mGridImage.getY();
				b.getPixels(c, 0, bWidth, 0, 0, bWidth, bHeight);
				try {
					if (x + bWidth <= bitmap.getWidth()
							&& y + bHeight <= bitmap.getHeight()) {
						bitmap.setPixels(c, 0, bWidth, x, y, bWidth, bHeight);
					}
				} catch (IllegalArgumentException iae) {
					iae.printStackTrace();
					statusText = "Out of Boundary";
					Log.d("Out_of_boundary", "Bitmap:" + bitmap.getWidth()
							+ "X" + bitmap.getHeight());
					Log.d("Out_of_boundary", "x:" + x + " y:" + y + " bWidth:"
							+ bWidth + " bHeight:" + bHeight);
				}

			}
		}

	}

	/**
	 * Update the srcRect to achieve scale and scroll.
	 * 
	 * 
	 */
	private void updataSrcRect() {
		int left, top, right, bottom;
		bitmapHeight = bitmap.getHeight();
		bitmapWidth = bitmap.getWidth();
		int height = (int) (bitmapHeight / scale);
		int width = screenWidth * height / screenHeight;
		left = (int) (scrollX * bitmapWidth - width / 2);
		right = (int) (scrollX * bitmapWidth + width / 2);
		top = (int) (scrollY * bitmapHeight - height / 2);
		bottom = (int) (scrollY * bitmapHeight + height / 2);
		srcRect = new Rect(left, top, right, bottom);
		if (PROTOCAL == Setting.PROTOCAL_FIXED && mOnGridChangeListener != null) {
			checkIntersectRectsChange();

		}
		if (PROTOCAL == Setting.PROTOCAL_FLEXIBLE) {
			double updateScale = Setting.UPDATE_WINDOW_SCALE;
			updateRect = new Rect((int) (left - updateScale * width / 2),
					(int) (top - updateScale * height / 2),
					(int) (right + updateScale * width / 2),
					(int) (bottom + updateScale * height / 2));
			checkAboveThreshold();

		}

	}

	/**
	 * Just call OnWindowChange Method in all conditions.
	 */
	private void checkAboveThreshold() {

		int left = Math.max(0, updateRect.left);
		int right = Math.min(updateRect.right, bitmapWidth);

		int top = Math.max(0, updateRect.top);
		int bottom = Math.min(updateRect.bottom, bitmapHeight);

		if (true) {

			mOnGridChangeListener.OnWindowChange(left, top, right - left,
					bottom - top);

		}

	}

	/**
	 * Given rows and cols, initiate a list of rects that is used to determine
	 * which part of bitmap is shown. This should be called after setImageBitmap
	 * has been called.
	 * 
	 * @param rows
	 * @param cols
	 */
	public static void setGridDimension(int rows, int cols) {
		Log.d("Rect_Change", "Set Dimension:" + rows + " " + cols);
		mGridCols = cols;
		intersectRects = new boolean[rows * cols];
		mGridRects = new ArrayList<Rect>();
		int rectHeight = bitmapHeight / rows;
		int rectWidth = bitmapWidth / cols;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				int left = j * rectWidth;
				int right = (j + 1) * rectWidth;
				int top = i * rectHeight;
				int bottom = (i + 1) * rectHeight;
				Rect r = new Rect(left, top, right, bottom);
				mGridRects.add(r);
				intersectRects[cols * i + j] = true;
			}
		}
	}

	/**
	 * This will check any change of visible Rects and call back the
	 * OnRectChangeListener.
	 */
	private void checkIntersectRectsChange() {
		for (int i = 0; i < intersectRects.length; i++) {
			if (intersectRects[i] != (Rect.intersects(mGridRects.get(i),
					srcRect) || srcRect.contains(mGridRects.get(i)))) {
				intersectRects[i] = Rect.intersects(mGridRects.get(i), srcRect)
						|| srcRect.contains(mGridRects.get(i));

				mOnGridChangeListener.OnGridChange(i, intersectRects[i]);

			}
		}

	}

	/**
	 * This is the rendering thread that update the surfaceview.
	 * 
	 * @author YiH
	 * 
	 */
	public class MySurfaceThread extends Thread {
		private SurfaceHolder mSurfaceHolder;
		private boolean isRunning = false;

		public MySurfaceThread(SurfaceHolder sh, ScreenSurfaceView ssv) {
			mSurfaceHolder = sh;

		}

		public void startThread() {
			isRunning = true;
			super.start();
		}

		public void stopThread() {
			isRunning = false;
		}

		@Override
		public void run() {

			Canvas c = null;
			while (isRunning) {
				long startTimestamp = System.currentTimeMillis();
				c = null;
				try {
					c = mSurfaceHolder.lockCanvas();
					synchronized (mSurfaceHolder) {

						if (c != null) {
							draw(c);
						}
					}

				} finally {
					if (c != null) {
						mSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
				long timeCost = System.currentTimeMillis() - startTimestamp;
				if (Setting.RECORD_TIMECOST) {
					try {
						BluetoothImage.mBufferedWriter.append("Update,"
								+ timeCost);
						BluetoothImage.mBufferedWriter.newLine();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				// Log.d(TAG, "Time cost:"+timeCost);
				if (timeCost < SLEEP_TIME) {
					try {
						Thread.sleep(SLEEP_TIME - timeCost);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Start rendering thread.
	 */
	public void startRendering() {
		if (thread == null) {
			thread = new MySurfaceThread(getHolder(), this);
			thread.startThread();
		}
	}

	/**
	 * Stop rendering thread.
	 */
	public void stopRendering() {
		if (thread != null) {
			thread.stopThread();
			boolean retry = true;
			while (retry) {
				try {
					thread.join();
					retry = false;
				} catch (InterruptedException e) {

				}
			}
			thread = null;
		}
	}

	/**
	 * Given x and y which is the display size. This will determine the which
	 * area on the screen will be drawn. The display area will be a Rect start
	 * from (0,0).
	 * 
	 * 
	 * @param x
	 *            the width of display area
	 * @param y
	 *            the height of display area
	 */
	public void setScreenSize(int x, int y) {
		screenHeight = y;
		screenWidth = x;
		if (dstRect == null) {
			Log.d(TAG, "dstRct:" + 0 + " " + 0 + " " + screenWidth + " "
					+ screenHeight);

			dstRect = new Rect(0, 0, screenWidth, screenHeight);

		}

	}

	/**
	 * Create the bitmap that is used for canvas with given width and height.
	 * This should be called before setImageBitmap;
	 * 
	 * @param width
	 *            The width of bitmap used by canvas
	 * @param height
	 *            The height of bitmap used by canvas
	 */
	public static void initBitmap(int width, int height) {
		bitmapHeight = height;
		bitmapWidth = width;
		Log.d("FOCUS_TEST_GLASS", "!!!!!!!!!!!!Width and height!!!!!!!!!!!!"
				+ width + " " + height);
		portraitbitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight,
				Config.ARGB_8888);
		landscapeBitmap = Bitmap.createBitmap(bitmapHeight, bitmapWidth,
				Config.ARGB_8888);
		bitmap = portraitbitmap;
	}

	/**
	 * @return X coordinate of the center of the Screen in Bitmap
	 */
	public float getX() {
		if (srcRect != null) {
			return (srcRect.left + srcRect.right) / 2;
		}
		return 0;
	}

	/**
	 * 
	 * @return Y coordinate of the center of the Screen in Bitmap
	 */
	public float getY() {
		if (srcRect != null) {
			return (srcRect.top + srcRect.bottom) / 2;
		}
		return 0;
	}

	public void switchMode() {
		if (Setting.isLandScape) {
			portraitbitmap = bitmap;
			bitmap = landscapeBitmap;
		} else {
			landscapeBitmap = bitmap;
			bitmap = portraitbitmap;
		}

	}

}
