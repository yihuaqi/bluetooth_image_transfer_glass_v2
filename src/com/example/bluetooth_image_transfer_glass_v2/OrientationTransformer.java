package com.example.bluetooth_image_transfer_glass_v2;

/**
 * Helper class for converting Orientation data within a range into relative
 * position.
 * 
 * @author YiH
 * 
 */
public class OrientationTransformer {
	double center_azimuth;
	double center_pitch;
	double center_roll;

	// Half of the Rectangle Width and Height.
	double half_width = Setting.FIXED_WIDTH;
	double half_height = Setting.FIXED_HEIGHT;
	int image_width = 480;
	int image_height = 800;
	int screen_width;
	int screen_height;
	static OrientationTransformer instance;

	private OrientationTransformer() {

	}

	static public OrientationTransformer getInstance() {
		if (instance == null) {
			instance = new OrientationTransformer();
		}
		return instance;
	}

	public void setScreenSize(int width, int height) {
		screen_width = width;
		screen_height = height;
	}

	public void setImageSize(int width, int height) {
		// image_width = width;
		// image_height = height;
		image_height = height;
		image_width = width;
	}

	/**
	 * Set the current orientation value as center
	 * 
	 * @param orientation_azimuth
	 * @param orientation_pitch
	 * @param orientation_roll
	 */
	public void setCenter(double orientation_azimuth, double orientation_pitch,
			double orientation_roll) {
		center_azimuth = orientation_azimuth;
		center_pitch = orientation_pitch;
		center_roll = orientation_roll;

	}

	/**
	 * Set the current orientation value as corner
	 * 
	 * @param orientation_azimuth
	 * @param orientation_pitch
	 * @param orientation_roll
	 */
	public void setCorner(double orientation_azimuth, double orientation_pitch,
			double orientation_roll) {
		half_width = Math.abs(orientation_azimuth - center_azimuth);
		half_height = Math.abs(orientation_pitch - center_pitch);

	}

	/**
	 * Given a azimuth, returns a relative position of X. The range is from 0 to
	 * 1. 0 means the left margin and 1 means the right margin. X is the x-axis
	 * coordinate of the center of screen.
	 * 
	 * @param orientation_azimuth
	 * @return relative position for x-axis ranging from 0 to 1.
	 */
	public double getRelativePositionX(double orientation_azimuth) {
		return Math.max(
				Math.min((1 - center_azimuth / half_width) / 2.0
						+ orientation_azimuth / (2 * half_width), 1), 0);

	}

	/**
	 * Given a pitch, returns a relative position of Y. The range is from 0 to
	 * 1. 0 means the top margin and 1 means the bottom margin. Y is the y-axis
	 * coordinate of the center of the screen.
	 * 
	 * @param orientation_pitch
	 * @return relative position for y-axis ranging from 0 to 1.
	 */
	public double getRelativePositionY(double orientation_pitch) {
		if (Setting.FOR_LEFT_EYE) {
			return 1 - Math.max(
					Math.min((1 - center_pitch / half_height) / 2.0
							+ orientation_pitch / (2 * half_height), 1), 0);
		} else {
			return Math.max(
					Math.min((1 - center_pitch / half_height) / 2.0
							+ orientation_pitch / (2 * half_height), 1), 0);
		}

	}

}
