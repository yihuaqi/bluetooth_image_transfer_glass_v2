This is the Glass side of Google Glass screen-sharing app.

**Description of Google Glass screen-sharing app:**
With the Google Glass screen-sharing app, we are proposing a radically different way to visualize and interact 
with smartphones, and it could be especially beneficial when the mobile screen is magnified for easy accessibility. 
The Google Glass screen sharing app can project the screen of the paired mobile phone on Google Glass display 
at an appropriate magnification. The user, wearing the Google Glass, can view the projected smartphone screen 
by moving the head from left to right and top to bottom, and interact with it by tapping on the Google Glass side 
panel. Hence, instead of scrolling with hand gestures, the magnified smartphone screen can be conveniently seen 
on the Google glass display by moving the head.